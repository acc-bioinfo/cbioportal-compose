
# cBioportal with AD and docker compose

This project allows to start a local cBioPortal instance with docker compose. Authentication is done by default with Active Directory. This work is derived from the cbioportal-docker project developped by The Hyve https://github.com/thehyve/cbioportal-docker

## Configure


### portal.properties

By default, the cbioportal container will be built with the property file config/ad.portal.properties. You should edit this file, setting at least active directory properties. If you are not using ActiveDirectory for authentication, you should also update the CATALINA_OPTS authenticate parameter in `docker-compose.yaml`:

```
CATALINA_OPTS: '-Xms2g -Xmx4g  -Dauthenticate=ad' 
```

The methods allowed are: googleplus, saml, openid, ad, ldap, noauthsessionservice.

Note that by default, the database will be created in a docker volume. If you prefer to mount a local directory, update `docker-compose.yaml`, for instance by removing the lines:

```
volumes:
    cbioportal-db: {}
```

and replacing ` - cbioportal-db:/var/lib/mysql ` by ` - ./cbioportal-db:/var/lib/mysql`

### DB credentials

If you want to change the default credentials to access the DB (recomended in production), you should change it in :
- docker-compose.yaml
- ad-portal.propertied



## Start

### Init the DB: 





## Update the schema:

If you need to update the schema, you will have to run the command `migrate_db.py --properties-file src/main/resources/portal.properties --sql db-scripts/src/main/resources/migration.sql`

With docker compose, do it directly with :

```
docker-compose exec web migrate_db.py --properties-file src/main/resources/portal.properties --sql db-scripts/src/main/resources/migration.sql
```
